<?php
    namespace dtw;

    class Search {
        public static function getForm($value = null) {
            $form = new \dtw\utils\Form('Search', '<i class="fas fa-search"></i>', array(
                'hideTitle' => true,
                'action' => '/search',
                'block' => false
            ));
            $form->addField('Search', 'text', array(
                'placeholder' => 'Search...',
                'hideLabel' => true,
                'name' => 'q',
                'value' => $value
            ));

            return $form;
        }

        public function __construct() {
            if (!\dtw\DtW::$config->get('elasticsearch')) {
                throw new \Exception('Elasticsearch not enabled');
            }

            $this->client = \Elasticsearch\ClientBuilder::create()->setHosts([\dtw\DtW::$config->get('elasticsearch')])->build();
        }

        public function reseed() {
            $DtW = \dtw\DtW::getInstance();

            // Delete all
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, \dtw\DtW::$config->get('elasticsearch') . '/*');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            $result = curl_exec($ch);

            // Pages
            $params = [
                'index' => 'pages',
                'body' => [
                    'mappings' => [
                        'properties' => [
                            'title' => [
                                'type' => 'text'
                            ],
                            'section' => [
                                'type' => 'text'
                            ],
                            'url' => [
                                'type' => 'text'
                            ]
                        ]
                    ]
                ]
            ];
            $this->client->indices()->create($params);

            $this->client->index([
                'index' => 'pages',
                'body'  => ['title' => 'Settings', 'url' => '/settings']
            ]);

            // Index help pages
            $help = \dtw\Help::getIndex();
            foreach ($help AS $section => $items) {
                $section = sprintf('%s > %s', 'Help', $section);
                foreach ($items AS $item) {
                    $title = $item->title;
                    $this->client->index([
                        'index' => 'pages',
                        'body'  => [
                            'title' => $title,
                            'section' => $section,
                            'url' => $item->permalink
                        ]
                    ]);
                }
            }

            // Index settings pages
            $settings = \dtw\Settings::getIndex();
            foreach ($settings AS $section => $items) {
                $section = sprintf('%s > %s', 'Settings', $section);
                foreach ($items AS $item) {
                    $title = $item['title'];
                    $this->client->index([
                        'index' => 'pages',
                            'body'  => ['title' => $title,
                            'section' => $section,
                            'url' => $item['permalink']
                        ]
                    ]);
                }
            }

            // Users
            $params = [
                'index' => 'users',
                'body' => [
                    'mappings' => [
                        'properties' => [
                            'username' => [
                                'type' => 'text'
                            ]
                        ]
                    ]
                ]
            ];
            $this->client->indices()->create($params);

            $params = array();
            $sql = 'SELECT `user_id` AS `id`, `username` FROM users';
            $stmt = \dtw\DtW::$db->prepare($sql); 
            $stmt->execute(array()); 
            while ($row = $stmt->fetch()) {
                $params['body'][] = [
                  'index' => [
                    '_index' => 'users',
                    '_id' => $row->id,
                  ]
                ];
                $params['body'][] = ['username' => $row->username];

                if (count($params['body']) > 10000) {
                    $this->client->bulk($params);
                    $params = array();
                }
            }
            $this->client->bulk($params);


            // Articles
            $params = [
                'index' => 'articles',
                'body' => [
                    'mappings' => [
                        'properties' => [
                            'title' => [
                                'type' => 'text',
                                'boost' => 3
                            ],
                            'content' => [
                                'type' => 'text'
                            ],
                            'published' => [
                                'type' => 'date',
                                'index' => false
                            ]
                        ]
                    ]
                ]
            ];
            $this->client->indices()->create($params);

            $params = array();
            $sql = 'SELECT `article_id` AS `id`, `title`, `content`, `published` FROM articles WHERE `status` = "published"';
            $stmt = \dtw\DtW::$db->prepare($sql); 
            $stmt->execute(array()); 
            while ($row = $stmt->fetch()) {
                $params['body'][] = [
                  'index' => [
                    '_index' => 'articles',
                    '_id' => $row->id,
                  ]
                ];
                $params['body'][] = ['title' => $row->title, 'content' => $row->content, 'published' => strtotime($row->published)];
            }

            $this->client->bulk($params);


            // Discussion posts
            $params = [
                'index' => 'discussions',
                'body' => [
                    'mappings' => [
                        'properties' => [
                            'title' => [
                                'type' => 'text',
                                'boost' => 3
                            ]
                        ]
                    ]
                ]
            ];
            $this->client->indices()->create($params);

            $params = array();
            $sql = 'select thread.thread_id as `id`, thread.title from forum_threads thread where thread.deleted = 0;';
            $stmt = \dtw\DtW::$db->prepare($sql); 
            $stmt->execute(array()); 
            while ($row = $stmt->fetch()) {

                $sql2 = 'SELECT `message` FROM forum_thread_posts WHERE `thread_id` = :thread AND `deleted` = 0';
                $stmt2 = \dtw\DtW::$db->prepare($sql2); 
                $stmt2->execute(array( 'thread' => $row->id )); 
                $posts = $stmt2->fetchAll(\PDO::FETCH_COLUMN);

                $params['body'][] = [
                  'index' => [
                    '_index' => 'discussions',
                    '_id' => $row->id,
                  ]
                ];
                $params['body'][] = ['title' => $row->title, 'content' => $posts];

                if (count($params['body']) > 10000) {
                    try {
                        $this->client->bulk($params);
                        $params = array();
                    } catch (\Exception $e) {
                        var_dump($params);
                        die();
                    }
                }
            }
            $this->client->bulk($params);
        }

        public function getResults($term) {
            if (strlen($term) < 3) {
                throw new \Exception('Search term needs to be 3 characters or longer');
            }

            $results = array();

            $results['pages'] = $this->getElasticResults($term, 'pages', 5, [ "title", "section", "url" ]);
            $results['users'] = $this->getElasticResults($term, 'users', 10);
            $results['articles'] = $this->getElasticResults($term, 'articles', 5);
            $results['discussions'] = $this->getElasticResults($term, 'discussions');

            if (!count($results['users']) && !count($results['articles']) && !count($results['discussions'])) {
                throw new \Exception('No results found');
            }

            return $results;
        }

        public function getElasticResults($term, $index, $limit = 15, $source = [ "title" ]) {
            $class = new \stdClass();

            $params = [
                'index' => [ $index ],
                'body'  => [
                    "size" => $limit,
                    "_source" => $source,
                    'query' => [
                        "multi_match" => [
                            "query" => $term,
                            "fields" => ["title^3", "content", "username"],
                            "fuzziness" => "AUTO"
                        ]
                    ],
                    "highlight" => [
                        "fields" => [
                            "title" => $class
                        ]
                    ]
                ]
            ];

            try {
                $response = $this->client->search($params);
            } catch (\Exception $e) {
                throw $e;
            }

            $results = $response['hits']['hits'];
            $results = $this->convertResults($results);

            return $results;
        }

        private function convertresults($data) {
            $results = array();

            foreach($data AS $item) {
                // echo $item['_index'] . ' : ' . $item['_id'] . "\n";

                $type = $item['_index'];
                $id = $item['_id'];

                try {
                    switch ($type) {
                        case 'articles': $result = Articles::getArticle($id); break;
                        case 'users': $result = Users::getUser($id); break;
                        case 'discussions': $result = new \dtw\discussion\Thread($id); break;
                        case 'pages': $result = $item['_source']; break;
                        default: throw new \Exception('Invalid index');
                    }

                    array_push($results, $result); 
                } catch (\Exception $e) {
                    // Item index is invalid or item doesn't exist
                }     
            }

            return $results;
        }

        public function info() {
            $response = $this->client->indices()->stats();
            $stats = new \stdClass();

            $total = $response['_all']['total'];
            $stats->total = [
                'size' => $total['store']['size_in_bytes'],
                'searches' => $total['search']['query_total'],
            ];

            $stats->indices = [];
            foreach($response['indices'] AS $key => $index) {
                $stats->indices[$key] = [
                    'items' => $index['total']['docs']['count']
                ];
            }

            return $stats;
        }
    }
