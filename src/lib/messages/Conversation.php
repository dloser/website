<?php
    namespace dtw\messages;

    class Conversation extends \dtw\utils\DataObject {
        protected $type = 'pm:conversation';

        public function __construct($id) {
            $conversation = \dtw\DtW::$redis->get($this->type . ':' . $id);
            if ($conversation) {
                $this->data = json_decode($conversation);

                // Check user access
                if (!in_array(\dtw\DtW::getInstance()->user->id, $this->data->participants)) {
                    throw new \Exception('Conversation not found');
                }
            } else {
                // Check user has access
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT pm_messages.pm_id AS `id`, pm_messages.message AS latest, pm_messages.user_id AS latestSender, pm_messages.time AS `updated`
                    FROM pm_users
                    LEFT JOIN pm_messages
                        ON message_id = (SELECT message_id FROM pm_messages WHERE pm_messages.pm_id = pm_users.pm_id ORDER BY time DESC LIMIT 1)
                    WHERE pm_users.user_id = :userID AND pm_users.pm_id = :pmID
                '); 
                $stmt->execute(array(':userID' => \dtw\DtW::getInstance()->user->id, ':pmID' => $id));
                if (!$stmt->rowCount()) {
                    throw new \Exception('Conversation not found');
                }

                $this->data = $stmt->fetch();

                $this->id = $this->data->id;
                $this->load();
            }

            $this->id = $this->data->id;
            $this->loadUserSpecific();

            if (count($this->getMessages()) == 0) {
                throw new \Exception('Conversation has been deleted');
            }
        }

        public function load() {
            if ($this->data->participants) {
                return;
            }

            // Load participants
            $stmt = \dtw\DtW::$db->prepare('
                SELECT pm_users.user_id
                FROM pm_users
                WHERE pm_users.pm_id = :pmID
            '); 
            $stmt->execute(array(
                ':pmID' => $this->id
            ));
            $this->data->participants = $stmt->fetchAll(\PDO::FETCH_COLUMN);

            // Load messages
            $stmt = \dtw\DtW::$db->prepare('
                SELECT pm_messages.message, pm_messages.time AS `sent`, pm_messages.user_id AS `from`
                FROM pm_messages
                WHERE pm_id = :pmID
                ORDER BY `time`
            '); 
            $stmt->execute(array(
                ':pmID' => $this->id
            ));

            $this->data->messages = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\\Message');

            // Get users details - wont be cached
            $this->loadUserSpecific();

            $this->updateCache();
        }

        private function loadUserSpecific() {
            $key = 'pm:user:conversations:' . \dtw\DtW::getInstance()->user->id;

            $conversations = \dtw\DtW::$redis->get($key);
            if ($conversations) {
                $all = json_decode($conversations);
                $index = array_search($this->id, array_column($all, 'id'));

                if ($index !== false) {
                    $data = $all[$index];
                }
            } 

            if (!$data) {
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT pm_users.pm_id AS `id`, pm_users.seen, pm_users.deleted
                    FROM pm_users
                    LEFT JOIN pm_messages
                        ON message_id = (SELECT message_id FROM pm_messages WHERE pm_messages.pm_id = pm_users.pm_id ORDER BY time DESC LIMIT 1)
                    WHERE pm_users.user_id = :userID AND pm_users.pm_id = :pmID
                    ORDER BY `pm_messages`.time DESC
                    LIMIT 0, 15
                '); 
                $stmt->execute(array(
                    ':userID' => \dtw\DtW::getInstance()->user->id,
                    ':pmID' => $this->id
                ));
                $data = $stmt->fetch();
            }

            $this->seen = $data->seen;
            $this->deleted = $data->deleted;
        }

        public function getMessages() {
            if (!$this->deleted) {
                return $this->data->messages;
            }

            return array_filter($this->data->messages, function($message) {
                return $message->sent > $this->deleted;
            });
        }

        public function markAsDeleted() {
            // Mark conversation as seen
            $stmt = \dtw\DtW::$db->prepare('
                UPDATE pm_users
                SET deleted = now()
                WHERE pm_users.user_id = :userID AND pm_users.pm_id = :pmID
            '); 
            $stmt->execute(array(
                ':userID' => \dtw\DtW::getInstance()->user->id,
                ':pmID' => $this->id
            ));

            // Update user cache
            $key = 'pm:user:conversations:' . \dtw\DtW::getInstance()->user->id;
            \dtw\DtW::$redis->del($key);
            $key = 'pm:user:unread:' . \dtw\DtW::getInstance()->user->id;
            \dtw\DtW::$redis->del($key);
        }

        public function markAsRead() {
            if ($this->data->updated < $this->seen) {
                return;
            }

            // Mark conversation as seen
            $stmt = \dtw\DtW::$db->prepare('
                UPDATE pm_users
                SET seen = now()
                WHERE pm_users.user_id = :userID AND pm_users.pm_id = :pmID
            '); 
            $stmt->execute(array(
                ':userID' => \dtw\DtW::getInstance()->user->id,
                ':pmID' => $this->id
            ));

            // Update user cache
            $key = 'pm:user:conversations:' . \dtw\DtW::getInstance()->user->id;
            \dtw\DtW::$redis->del($key);
            $key = 'pm:user:unread:' . \dtw\DtW::getInstance()->user->id;
            \dtw\DtW::$redis->del($key);
        }

        public function getLatestMessage() {
            $latest = new \stdClass();

            $latest->preview = htmlspecialchars($this->data->latest);

            if ($this->data->latestSender != \dtw\DtW::getInstance()->user->id) {
                $latest->from = $this->data->latestSender;
            } else {
                $this->load();

                if (!count($this->participants)) {
                    $latest->from = \dtw\DtW::getInstance()->user->id;
                } else {
                    $latest->from = $this->participants[0];
                }
            }

            return $latest;
        }

        public function addReply($message) {
            if (!$message) {
                throw new \Exception("Message is required");
            }

            foreach ($this->participants AS $participant) {
                try {
                    $profile = \dtw\Users::getUser($participant);
                } catch (\Exception $e) {
                    continue;
                }
                if ($profile->isBlocked()) {
                    throw new \Exception('You can\'t messages blocked users');
                } elseif ($profile->hasBlocked()) {
                    throw new \Exception('You are unable to reply to this conversation');
                }
            }

            // Add message
            $stmt = \dtw\DtW::$db->prepare('INSERT INTO `pm_messages` (`pm_id`, `user_id`, `message`) VALUES (:id, :userID, :message)'); 
            $stmt->execute(array(
                ':id' => $this->id,
                ':userID' => \dtw\DtW::getInstance()->user->id,
                ':message' => $message
            ));

            // Clear conversation cache
            \dtw\DtW::$redis->del($this->type . ':' . $this->data->id);

            // Send notifications
            $notificationData = (object) array(
                'conversation' => $this->id
            );
            foreach ($this->participants AS $participant) {
                if ($participant !== \dtw\DtW::getInstance()->user->id) {
                    \dtw\Notifications::send($participant, 'message.new', $notificationData);
                }

                // Clear their conversation cache
                $key = 'pm:user:conversations:' . $participant;
                \dtw\DtW::$redis->del($key);
                $key = 'pm:user:unread:' . $participant;
                \dtw\DtW::$redis->del($key);
            }
        }
    }