<?php
    namespace dtw\moderation;

    class Articles {

        public function __construct() {
            if (!\dtw\DtW::getInstance()->user->hasPrivilege('articles.publish')) {
                throw new \Exception('Invalid permissions');
            }

            $this->queues = array();
            $this->queues['new'] = array(
                'title' => 'New submissions',
                'description' => 'Review submitted articles',
                'privilege' => 'articles.publish'
            );
        }

        public function getNewArticles() {
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `article_id` AS `id`
                FROM `articles`
                WHERE `status` = \'review\'
                ORDER BY `created` DESC;
            ');
            $stmt->execute(); 

            return $stmt->fetchAll(\PDO::FETCH_COLUMN);
        }

        public function getQueues() {
            $this->queues['new']['count'] = count($this->getNewArticles());

            foreach($this->queues AS &$queue) {
                $queue['access'] = \dtw\DtW::getInstance()->user->hasPrivilege($queue['privilege']);
            }

            return $this->queues;
        }

        public function getLogs() {
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `revision_id`, `article_id`, `log`, `reviewer`, `reviewed`, `status`
                FROM `article_revisions`
                WHERE `reviewer` IS NOT NULL
                ORDER BY `reviewed` DESC
            ');
            $stmt->execute(); 
            if ($stmt->rowCount()) {
                $logs = $stmt->fetchAll();
            }

            return $logs;
        }

    }