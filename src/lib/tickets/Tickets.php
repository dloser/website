<?php
    namespace dtw\tickets;

    class Tickets {

        public static function new($data) {
            $DtW = \dtw\DtW::getInstance();

            if (!isset($data['message']) || !$data['message']) {
                throw new \Exception('Message missing');
            }

            $token = null;
            if ($DtW->user->isAuth()) {
                $data['user_id'] = $DtW->user->id;
                $data['email'] = null;
            } else if (!isset($data['email']) || !$data['email']) {
                throw new \Exception('Invalid email');
            } else {
                $data['user_id'] = null;
                $token = \dtw\utils\Utils::generateToken();
            }

            \dtw\DtW::$db->beginTransaction();

            try {
                // Add to records
                $stmt = \dtw\DtW::$db->prepare('INSERT INTO tickets (`user_id`, `email`, `token`) VALUES (:userID, :email, :token)');
                $stmt->execute(array(
                    ':userID' => $data['user_id'],
                    ':email' => $data['email'],
                    ':token' => $token
                ));

                $ticketID = \dtw\DtW::$db->lastInsertId();

                $ticket = \dtw\tickets\Tickets::get($ticketID, true);
                $ticket->addMessage($data['message']);

                \dtw\DtW::$db->commit();
            } catch (\Exception $e) {
                \dtw\DtW::$db->rollback();
                throw new \Exception('Error creating ticket');
            }

            $url = '/help/contact/' . $ticketID;
            if (!$DtW->user->isAuth()) {
                $url .= '?token=' . $token;
            }

            return $url;
        }

        public static function get($ticketID, $force = false) {
            return new Ticket($ticketID, $force);
        }

        public static function getMyTickets() {
            $DtW = \dtw\DtW::getInstance();

            $stmt = \dtw\DtW::$db->prepare("
                SELECT tickets.`ticket_id`
                FROM tickets
                LEFT JOIN `ticket_messages`
                ON `ticket_messages`.`ticket_id` = tickets.`ticket_id`
                WHERE `ticket_messages`.`sent` = (SELECT MAX(`sent`) FROM ticket_messages WHERE `ticket_id` = tickets.`ticket_id`)
                    AND tickets.`user_id` = :userID
                ORDER BY
                    FIELD(status, 'open', 'resolved', 'closed'),
                    IF(`tickets`.`user_id` = `ticket_messages`.`user_id`, 1, 0) DESC,
                    `ticket_messages`.`sent` DESC
            ");
            $stmt->execute(array(
                ':userID' => $DtW->user->id
            )); 
            $tickets = $stmt->fetchAll();
            foreach($tickets AS &$ticket) {
                $ticket = self::get($ticket->ticket_id);
            }

            return $tickets;
        }

        public static function getAllTickets($status = null) {
            $DtW = \dtw\DtW::getInstance();

            if (!$DtW->user->hasPrivilege('tickets')) {
                throw new \Exception('No access');
            }

            $params = array();
            if ($status) {
                $extra = ' AND `tickets`.`status` = :status ';
                $params[':status'] = $status;
            }

            $stmt = \dtw\DtW::$db->prepare("
                SELECT tickets.`ticket_id`
                FROM tickets
                LEFT JOIN `ticket_messages`
                ON `ticket_messages`.`ticket_id` = tickets.`ticket_id`
                WHERE `ticket_messages`.`sent` = (SELECT MAX(`sent`) FROM ticket_messages WHERE `ticket_id` = tickets.`ticket_id`)
                    {$extra}
                ORDER BY
                    FIELD(status, 'open', 'resolved', 'closed'),
                    IF(`tickets`.`user_id` = `ticket_messages`.`user_id`, 1, 0) DESC,
                    `ticket_messages`.`sent` DESC
            ");
            $stmt->execute($params); 
            $tickets = $stmt->fetchAll();
            foreach($tickets AS &$ticket) {
                $ticket = self::get($ticket->ticket_id);
            }

            return $tickets;
        }
    }