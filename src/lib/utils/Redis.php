<?php
    namespace dtw\utils;

    class Redis {
        public function __construct() {
            $config = \dtw\DtW::$config->get('redis.server');

            $this->client = $client = new \Predis\Client($config, ['prefix' => \dtw\DtW::$config->get('redis.prefix') .':']);
        }
    }
?>