<?php
    namespace dtw\discussion;

    class Thread extends \dtw\utils\DataObject {
        protected $type = 'discussion';
        protected $table = 'forum_threads';

        protected function getBySlug($slug) {
            // Lookup slug in Redis
            $threadID = \dtw\DtW::$redis->get($this->type . ':slug:' . $slug);

            // If the link wasn't found, check DB
            if (!$threadID) {
                $stmt = \dtw\DtW::$db->prepare("SELECT thread_id FROM {$this->table} WHERE `slug` = :slug"); 
                $stmt->execute(array(':slug' => $slug)); 
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();
                    $threadID = $row->thread_id;

                    \dtw\DtW::$redis->set($this->type . ':slug:' . $slug, $threadID);
                } else {
                    throw new \Exception('Discussion thread not found');
                }
            }

            try {
                $this->getByID($threadID);
            } catch (\Exception $e) {
                throw $e;
            }
        }

        protected function getByID($id) {
            // Check if thread exists in Redis
            $thread = \dtw\DtW::$redis->get($this->type . ':' . $id);

            if ($thread) {
                $this->data = json_decode($thread);
                \dtw\DtW::$log->info('thread.view', array('title' => $this->data->title, 'source' => 'redis'));

                if (!$this->data->post_count) {
                    $this->loadMeta();
                }
            } else {
                // Rebuild thread from DB
                $stmt = \dtw\DtW::$db->prepare("
                    SELECT `thread_id` AS `id`, `slug`, {$this->table}.`title`, `sticky`, `locked`, `spoiler`, `author`, `topic_id`, `views`, `answer`, `level_id`
                    FROM {$this->table}
                    WHERE `thread_id` = :threadID AND `deleted` = 0"); 
                $stmt->execute(array(':threadID' => $id)); 
                if (!$stmt->rowCount()) {
                    throw new \Exception('Discussion thread not found');
                }
                $this->data = $stmt->fetch();

                $this->loadMeta();

                $this->updateCache();
                \dtw\DtW::$log->info('thread.view', array('title' => $this->data->title, 'source' => 'DB'));
            }

            $this->id = $this->data->id;

            // Can current user edit this post?
            $DtW = \dtw\DtW::getInstance();
            $this->isAuthor = ($this->data->author == $DtW->user->id);
            if ($this->isAuthor || $DtW->user->hasPrivilege('discussions.post_edit')) {
                $this->isEditable = true;
            }

            if ($this->data->deleted == 1) {
                $this->isEditable = false;
            }
        }

        public function hasAccess() {
            $DtW = \dtw\DtW::getInstance();
            if ($DtW->user->isAuth() && $DtW->user->hasPrivilege('green')) {
                return true;
            }

            if ($this->data->level_id && $this->data->topic_id == 8) {
                return (
                    $DtW->user->isAuth() &&
                    $DtW->user->levels->{$this->data->level_id}->hasCompleted
                );
            }

            return true;
        }

        public function isSpoiler() {
            if (!$this->data->spoiler) {
                return false;
            }

            if ($this->data->level_id) {
                $DtW = \dtw\DtW::getInstance();
                return !(
                    $DtW->user->isAuth() &&
                    $DtW->user->levels->{$this->data->level_id}->hasCompleted
                );
            }

            return true;
        }

        public function loadMeta() {
            // Get created and last updated
            $stmt = \dtw\DtW::$db->prepare("
                SELECT MIN(`posted`) AS `created`, COUNT(*) - 1 AS `post_count`
                FROM forum_thread_posts
                WHERE `thread_id` = :threadID AND `deleted` = 0");

            $stmt->execute(array(':threadID' => $this->data->id)); 
            if ($stmt->rowCount()) {
                $data = $stmt->fetch();
                $this->data = (object) array_merge((array) $this->data, (array) $data);

                $this->data->post_count = $this->data->post_count < 0 ? 0 : $this->data->post_count;
            }

            // Get last updated
            $stmt = \dtw\DtW::$db->prepare("
                SELECT `posted` AS `updated`, `author` AS `updated_by`
                FROM forum_thread_posts
                WHERE `thread_id` = :threadID AND `deleted` = 0
                ORDER BY `posted` DESC
                LIMIT 1");

            $stmt->execute(array(':threadID' => $this->data->id)); 
            if ($stmt->rowCount()) {
                $data = $stmt->fetch();
                $this->data = (object) array_merge((array) $this->data, (array) $data);
            }

            // Get topic
            $DtW = \dtw\DtW::getInstance();
            $DtW->load('Discussions');
            $this->data->topic = $DtW->discussions->getTopic($this->data->topic_id);

            if ($this->data->topic->parent) {
                $this->data->topic->parent = $DtW->discussions->getTopic($this->data->topic->parent);
            }

            // Set permalink
            $this->data->permalink = \dtw\DtW::$config->get('site.domain') . '/discussion/' . $this->data->slug;

            $this->updateCache();
        }

        public function loadPosts() {
            $this->data->posts = array();

            $stmt = \dtw\DtW::$db->prepare('
                SELECT `post_id` AS `id`
                FROM `forum_thread_posts`
                WHERE `thread_id` = :threadID
            '); 
            $stmt->execute(array(':threadID' => $this->data->id));

            if ($stmt->rowCount()) {
                $posts = $stmt->fetchAll();

                foreach($posts AS &$post) {
                    array_push($this->data->posts, $post->id);
                }
            } else {
                // Mark thread as deleted
                throw new \Exception('Discussion topic not found');
            }

            // Update cache
            $this->updateCache();
        }

        public function getPosts($force = false) {
            if ($force || (!isset($this->data->posts) || !count($this->data->posts))) {
                $this->loadPosts();
            }

            $this->posts = array();

            foreach($this->data->posts AS $post) {
                $p = new \dtw\discussion\Post($post);
                array_push($this->posts, $p);
            }

            $this->posts[0]->root = true;

            $this->stats = $this->getStats();
        }

        public function addPost($data) {
            $DtW = \dtw\DtW::getInstance();

            // Can user post
            if (!$DtW->user->isAuth()) {
                throw new \Exception("You do not have privileges to add a post to this thread");
            }

            if (!$this->hasAccess()) {
                throw new \Exception("Access denied");
            }

            if ($this->data->locked) {
                throw new \Exception("Discussion thread is locked for new posts");
            }

            $post = new \dtw\discussion\Post();
            $postID = $post->create($this->id, $data);

            // Clear cache on thread
            $this->clearCache();

            $notified = array();
            $notificationData = (object) array(
                'thread' => $this->id,
                'post' => $postID
            );

            // Notify mentioned users
            $mentions = \dtw\utils\Markdown::parseMentions($data['message']);
            if (count($mentions)) {
                foreach($mentions AS $mention) {
                    if ($mention->id === $DtW->user->id) {
                        continue;
                    }

                    try {
                        \dtw\Notifications::send($mention->id, 'discussion.mention', $notificationData);
                        array_push($notified, $mention->id);
                    } catch (\Exception $e) {
                        //
                    }
                }
            }

            // Notify participants
            $this->getPosts();
            $voices = $this->stats->voices;

            foreach($voices AS $voice) {
                if (!$voice || $voice === $DtW->user->id || in_array($voice, $notified)) {
                    continue; // Dont notify poster
                }

                try {
                    \dtw\Notifications::send($voice, 'discussion.reply', $notificationData);
                } catch (\Exception $e) {
                    //
                }
            }

            $this->buildSearch();

            // Add to feed
            \dtw\utils\Feed::addItemToFeed(array(
                'type' => 'discussion.post',
                'user' => $this->DtW->user->id,
                'thread' => $this->data->id
            ));
        }

        private function getStats() {
            $stats = new \stdClass();

            $stats->voices = array_unique(array_map(function($p) {
                return $p->author;
            }, $this->posts));

            // Find images
            $images = array();
            array_walk($this->posts, function($p, $key) use (&$images) {
                preg_match_all('/\<img (.*)src="([^"]+)"/i', $p->message->html, $matches);

                if (count($matches[2])) {
                    $images = array_merge($images, $matches[2]);
                }
            });
            $stats->images = array_unique($images);

            // Find videos
            $videos = array();
            array_walk($this->posts, function($p, $key) use (&$videos) {
                preg_match_all('/\<iframe (.*)src="([^"]+)"/i', $p->message->html, $matches);

                if (count($matches[2])) {
                    $videos = array_merge($videos, $matches[2]);
                }
            });
            $stats->videos = array_unique($videos);

            return $stats;
        }

        public function storeViews() {
            $key = $this->type . ':' . $this->data->id . ':views';
            $this->data->views += \dtw\DtW::$redis->sCard($key);

            $stmt = \dtw\DtW::$db->prepare("
                UPDATE {$this->table} SET `views` = :views WHERE `thread_id` = :threadID;
            ");
            $stmt->execute(array(
                ':threadID' => $this->data->id,
                ':views' => $this->data->views
            ));

            // Clear set
            \dtw\DtW::$redis->del($key);

            // Update cache
            $this->updateCache();
        }

        public function create($data) {
            if (!isset($data['title']) || empty($data['title']) || !isset($data['message']) || empty($data['message']) || empty($data['topic'])) {
                throw new \Exception('All fields are required');
            }

            $DtW = \dtw\DtW::getInstance();

            $data['slug'] = $this->generateSlug($data['title'], null, 'thread_id');

            // Check topic exists
            $topic = $DtW->discussions->getTopic($data['topic']);

            // If solutions auto mark as spoiler
            $spoiler = 0;
            if ($topic->title == 'Solutions') {
                $spoiler = 1;
            } else if ($data['spoilers'] == 1) {
                $spoiler = 1;
            }

            // Get level
            $levelID = null;
            if ($topic->title == 'Solutions' || $topic->title == 'Help') {
                if ($topic->title == 'Solutions' && !$DtW->user->levels->{$data['level']}->hasCompleted) {
                    throw new \Exception('You can only create solution threads for levels you have completed');
                }

                try {
                    $level = \dtw\Playground::getByID($data['level']);
                    $levelID = $level->ID;
                } catch (\Exception $e) {
                    // Invalid level
                }
            }

            $stmt = \dtw\DtW::$db->prepare("
                INSERT INTO {$this->table} (`title`, `slug`, `author`, `topic_id`, `spoiler`, `level_id`)
                VALUES (:title, :slug, :author, :topic_id, :spoiler, :level);
            ");
            $stmt->execute(array(
                ':title' => $data['title'],
                ':slug' => $data['slug'],
                ':author' => $DtW->user->id,
                ':topic_id' => $data['topic'],
                ':spoiler' => $spoiler,
                ':level' => $levelID
            ));

            $this->data = new \stdClass();
            $this->data->id = \dtw\DtW::$db->lastInsertId();
            $this->data->title = $data['title'];
            $this->data->slug = $this->data->id . '-' . $data['slug'];
            $this->data->permalink = \dtw\DtW::$config->get('site.domain') . '/discussion/' . $this->data->slug;
            $this->data->author = $DtW->user->id;
            $this->data->topic_id = $data['topic'];
            $this->data->spoiler = $spoiler;
            $this->data->level_id = $levelID;

            // Update slug
            $stmt = \dtw\DtW::$db->prepare("
                UPDATE {$this->table} SET `slug` = :slug WHERE `thread_id` = :threadID;
            ");
            $stmt->execute(array(
                ':threadID' => $this->data->id,
                ':slug' => $this->data->slug
            ));

            $this->updateCache();

            // Add to feed
            \dtw\utils\Feed::addItemToFeed(array(
                'type' => 'discussion.thread',
                'user' => $this->DtW->user->id,
                'thread' => $this->data->id
            ));

            return $this->data;
        }

        public function edit($key, $value) {
            if (!$this->isEditable) {
                throw new \Exception('Invalid request');
            }

            if ($key == 'level_id') {
                $levelID = null;
                $DtW = \dtw\DtW::getInstance();
                $topic = $DtW->discussions->getTopic($this->data->topic_id);

                if ($topic->title == 'Solutions' || $topic->title == 'Help') {
                    if ($topic->title == 'Solutions' && !$DtW->user->levels->{$value}->hasCompleted) {
                        throw new \Exception('You can only create solution threads for levels you have completed');
                    }

                    try {
                        $level = \dtw\Playground::getByID($value);
                        $levelID = $level->ID;
                    } catch (\Exception $e) {
                        // Invalid level
                    }
                }

                $stmt = \dtw\DtW::$db->prepare("
                    UPDATE {$this->table} SET `{$key}` = :value WHERE `thread_id` = :threadID;
                ");
                $stmt->execute(array(
                    ':value' => $levelID,
                    ':threadID' => $this->data->id
                ));
            } else {
                if (!in_array($key, array('title', 'topic_id', 'spoiler'))) {
                    throw new \Exception('Invalid request');
                }

                $stmt = \dtw\DtW::$db->prepare("
                    UPDATE {$this->table} SET `{$key}` = :value WHERE `thread_id` = :threadID;
                ");
                $stmt->execute(array(
                    ':value' => $value,
                    ':threadID' => $this->data->id
                ));

                $this->buildSearch();
            }

            // Remove from cache
            $this->clearCache();
        }

        public function delete() {
            if (!$this->isEditable) {
                throw new \Exception('Invalid request');
            }

            $stmt = \dtw\DtW::$db->prepare("
                UPDATE {$this->table} SET `deleted` = 1 WHERE `thread_id` = :threadID;
            ");
            $stmt->execute(array(
                ':threadID' => $this->data->id
            ));

            // Remove from cache
            $this->clearCache();
        }

        public function markAnswer($postID) {
            if (!$this->isAuthor) {
                throw new \Exception('Invalid request');
            }

            $stmt = \dtw\DtW::$db->prepare("
                UPDATE {$this->table} SET `answer` = :postID, `locked` = 1 WHERE `thread_id` = :threadID;
            ");
            $stmt->execute(array(
                ':postID' => $postID,
                ':threadID' => $this->data->id
            ));

            // Remove from cache
            $this->clearCache();
        }

        public function lock($lock = true) {
            if (!$this->isEditable) {
                throw new \Exception('Invalid request');
            }

            $stmt = \dtw\DtW::$db->prepare("
                UPDATE {$this->table} SET `locked` = :locked WHERE `thread_id` = :threadID;
            ");
            $stmt->execute(array(
                ':locked' => $lock ? 1 : 0,
                ':threadID' => $this->data->id
            ));  

            // Remove from cache
            $this->clearCache();
        }

        public function markAsSpoiler($spoiler = true) {
            if (!$this->isAuthor && !\dtw\DtW::getInstance()->user->hasPrivilege('discussions.flags')) {
                throw new \Exception('Invalid request');
            }

            $stmt = \dtw\DtW::$db->prepare("
                UPDATE {$this->table} SET `spoiler` = :spoiler WHERE `thread_id` = :threadID;
            ");
            $stmt->execute(array(
                ':spoiler' => $spoiler ? 1 : 0,
                ':threadID' => $this->data->id
            ));  

            // Remove from cache
            $this->clearCache();
        }

        public function awardedKarma() {
            $DtW = \dtw\DtW::getInstance();
            if (!$DtW->user->isAuth()) {
                return null;
            }

            $stmt = \dtw\DtW::$db->prepare("
                SELECT forum_karma.post_id, `vote`
                FROM forum_karma
                INNER JOIN forum_thread_posts
                ON forum_thread_posts.post_id = forum_karma.post_id
                WHERE user_id = :userID AND thread_id = :threadID;
            "); 
            $stmt->execute(array(
                ':userID' => $DtW->user->id,
                ':threadID' => $this->data->id
            ));
            
            $karma = $stmt->fetchAll();
            $data = array();

            foreach($karma AS $k) {
                $data[$k->post_id] = $k->vote;
            }

            return $data;
        }

        public function clearCache() {
            \dtw\DtW::$redis->del($this->type . 's');
            \dtw\DtW::$redis->del($this->type . ':' . $this->id);

            \dtw\DtW::$redis->del('discussions');
        }

        public function buildSearch() {
            \dtw\DtW::getInstance()->load('Search');

            // Update list of posts
            $this->getPosts(true);

            // Remove deleted posts
            $posts = array_filter($this->posts, function($post) { return !$post->deleted; });

            // Extract just the post content
            $posts = array_map(function($post) {
                return $post->message->plain;
            }, $posts);

            // Reset array keys so the list isn't converted to an object
            $posts = array_values($posts);

            $params = ['index' => 'discussions', 'id' => $this->id, 'body' => ['title' => $this->data->title, 'content' => $posts]];
            try {
                \dtw\DtW::getInstance()->search->client->index($params);
            } catch (\Exception $e) {
                //
            }
        }
    }