<?php
    namespace dtw\articles;

    class Topic extends \dtw\utils\DataObject {
        protected $type = 'topic';
        protected $table = 'article_topics';

        protected function getByslug($slug) {
            if ($slug == "drafts") {
                $this->getDrafts();
                return;
            }

            // Lookup slug in Redis
            $topicID = \dtw\DtW::$redis->get('articles:topic:slug:' . $slug);

            // If the link wasn't found, check DB
            if (!$topicID) {
                $stmt = \dtw\DtW::$db->prepare('SELECT topic_id FROM article_topics WHERE `slug` = :slug'); 
                $stmt->execute(array(':slug' => $slug)); 
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();
                    $topicID = $row->topic_id;

                    \dtw\DtW::$redis->set('articles:topic:slug:' . $slug, $topicID);
                } else {
                    throw new \Exception('Topic not found');
                }
            }

            try {
                $this->getByID($topicID);
            } catch (\Exception $e) {
                throw $e;
            }
        }

        protected function getByID($id) {
            // Check if topic exists in Redis
            $topic = \dtw\DtW::$redis->get($this->type . ':' . $id);

            if ($topic) {
                $this->data = json_decode($topic);
                \dtw\DtW::$log->info('topic.view', array('title' => $this->data->title, 'source' => 'redis'));
            } else {
                // Rebuild topic from DB
                $stmt = \dtw\DtW::$db->prepare('SELECT `topic_id` AS `id`, `slug`, `title`, `sidebar` FROM article_topics WHERE `topic_id` = :topicID'); 
                $stmt->execute(array(':topicID' => $id)); 
                if (!$stmt->rowCount()) {
                    throw new \Exception('Topic not found');
                }
                $this->data = $stmt->fetch();
                $this->generate();

                $this->updateCache();
                \dtw\DtW::$log->info('topic.view', array('title' => $this->data->title, 'source' => 'DB'));
            }

            $this->id = $this->data->id;
        }

        private function generate() {
            if ($this->data->thumbnail) {
                $this->data->thumbnail = \dtw\DtW::$config->get('site.static') . $this->data->thumbnail;
            }

            // Todo make hierarchical
            $this->data->permalink = '/articles/' . $this->data->slug;
            $this->data->titleSafe = htmlspecialchars($this->data->title);

            $this->getArticles();
        }

        private function getArticles() {
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `article_id`
                FROM `articles`
                WHERE `topic_id` = :topicID AND `status` = "published"
                ORDER BY `title` ASC;
            '); 
            $stmt->execute(array(':topicID' => $this->data->id)); 
            if ($stmt->rowCount()) {
                $this->data->articles = $stmt->fetchAll(\PDO::FETCH_COLUMN);
            }
        }

        private function getDraftArticles() {
            $DtW = \dtw\DtW::getInstance();

            $stmt = \dtw\DtW::$db->prepare('
                SELECT `article_id` AS `id`
                FROM `articles`
                WHERE `author` = :userID AND `status` != "published"
                ORDER BY `created` DESC;
            ');

            $stmt->execute(array(
                ':userID' => $DtW->user->id
            ));
            if ($stmt->rowCount()) {
                $this->data->articles = array();

                $articles = $stmt->fetchAll();
                foreach ($articles AS $article) {
                    try {
                        $article = new Article($article->id);
                        array_push($this->data->articles, $article);
                    } catch (\Exception $e) {
                        return false;
                    }
                }
            }
        }

        private function getDrafts() {
            $DtW = \dtw\DtW::getInstance();

            if (!$DtW->user->isAuth()) {
                throw new \Exception('You must be logged in');
            }

            $this->title = "Your articles";
            $this->slug = "drafts";
            $this->permalink = "/articles/drafts";
            $this->data = new \stdClass();

            $this->getDraftArticles();
        }

    }