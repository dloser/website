<?php
    namespace dtw\user;

    class Profile implements \JsonSerializable {
        private $_profile;

        public function __construct($id, $currentUser = false) {
            if ($id === null) {
                throw new \Exception('Profile not found');
            }

            if ($id && ctype_digit(strval($id))) {
                $this->id = $id;
            } else {
                // Lookup username
                try {
                    $this->id = $this->lookupUsername($id);
                } catch (\Exception $e) {
                    throw $e;
                }
            }

            $DtW = \dtw\DtW::getInstance();
            $this->currentUser = ($currentUser || (isset($DtW->user) && $this->id == $DtW->user->id));

            try {
                $this->loadProfile();
            } catch (\Exception $e) {
                throw $e;
            }
        }

        public function lookupUsername($username) {
            $id = \dtw\DtW::$redis->get('username:' . $username);

            if ($id) {
                return $id;
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT user_id
                FROM users
                WHERE `username` = :username'); 
            $stmt->execute(array(':username' => $username)); 
            if ($stmt->rowCount()) {
                $row = $stmt->fetch();

                $key = 'username:' . $username;
                \dtw\DtW::$redis->set($key, $row->user_id);

                // Set expiry
                \dtw\DtW::$redis->expire($key, 900);

                return $row->user_id;
            } else {
                throw new \Exception('Profile not found');
            }
        }

        public function jsonSerialize() {
            $details = clone $this->_profile;

            return $details;
        }

        public function __get($name) {
            if ($name == 'stats' && empty($this->_profile->stats)) {
                $this->loadStats();
            }

            if ($name == 'levels') {
                return $this->getLevels();
            }

            if ($name == 'levelProgress') {
                return $this->getLevelProgress();
            }

            if ($name == 'points') {
                return $this->calculatePoints();
            }

            if ($name == 'reputation') {
                return $this->calculateReputation();
            }

            if (property_exists($this->_profile, $name)) {
                return $this->_profile->$name;
            }

            return null;
        }

        public function __isset($name) {
            if ($name == 'stats' && empty($this->_profile->stats)) {
                $this->loadStats();
            }

            if ($name == 'link' || $name == 'levels' || $name == 'levelProgress' || $name == 'reputation' || $name == 'points') {
                return true;
            }

            return property_exists($this->_profile, $name);
        }

        public function getLink($extra = '', $args = '') {
            if ($this->isDonator()) {
                $extra .= ' data-donator';
            }

            if ($this->_profile->name && $this->_profile->name !== $this->_profile->username) {
                $name = sprintf("%s <span class='small c-subtle'>[%s]</span>", htmlspecialchars($this->_profile->name), $this->_profile->username);
            } else {
                $name = $this->_profile->username;
            }
            return sprintf("<a href='%s' %s data-profile-card='%s'>%s</a>", $this->_profile->permalink . $args, $extra, $this->_profile->username, $name );
        }

        public function getBio() {
            if (!isset($this->_profile->bio) || !$this->_profile->bio) {
                return;
            }

            if ($this->_profile->bioSafe) {
                return $this->_profile->bioSafe;
            }

            $purifier = new \dtw\utils\Purifier();
            $html = \dtw\utils\Markdown::parse($this->_profile->bio, false, false);
            $this->_profile->bioSafe = $purifier->purify($html);

            $this->updateCache();

            return $this->_profile->bioSafe;
        }

        public function getSignature() {
            if (!isset($this->_profile->signature) || !$this->_profile->signature) {
                return;
            }

            if ($this->_profile->signatureSafe) {
                return $this->_profile->signatureSafe;
            }

            $purifier = new \dtw\utils\Purifier();
            $html = \dtw\utils\Markdown::parse($this->_profile->signature, false, false);
            $this->_profile->signatureSafe = $purifier->purify($html);

            $this->updateCache();

            return $this->_profile->signatureSafe;
        }

        public function getMedals() {
            if (isset($this->_profile->medals) && is_array($this->_profile->medals)) {
                return $this->_profile->medals;
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT `medal_id`
                FROM user_medals
                WHERE `user_id` = :id
            ');
            $stmt->execute(array(':id' => $this->id));

            $this->_profile->medals  = $stmt->fetchAll(\PDO::FETCH_COLUMN);

            $this->updateCache();

            return $this->_profile->medals;
        }

        public function getMedalsGrouped() {
            $userMedals = $this->getMedals();

            // Get list of available medals
            $medalGroups = \dtw\Medals::getMedalsGrouped();

            $groups = array();
            foreach($medalGroups AS $group => $medalGroup) {
                foreach($medalGroup AS $medal) {
                    // Check if user has been awarded medal
                    if (in_array($medal->id, $userMedals)) {

                        if (!array_key_exists($group, $groups)) {
                            $groups[$group] = array();
                        }

                        array_push($groups[$group], $medal);
                    }
                }
            }

            return $groups;
        }

        private function checkMedals() {
            $medals = \dtw\Medals::getMedals();
            
            foreach($medals AS $medal) {
                try {
                    $medal->getUserProgress($this->id);
                } catch (\Exception $e) {
                    //
                }
            }
        }

        public function isDonator() {
            $medals = \dtw\Medals::getMedals();

            foreach ($medals as $medal) {
                if ($medal->type == 'donate') {
                    return $medal->hasCompleted($this->id);
                }
            }

            return false;
        }

        public function getFeed() {
            if (!$this->_profile->feedVisible) {
                return false;
            }

            $key = 'user:' . $this->id . ':feed';
            $feed = \dtw\DtW::$redis->get($key);
            if ($feed) {
                return json_decode($feed);
            }

            $feed = array();

            $feedItem = new \stdClass();
            $feedItem->type = 'joined';
            $feedItem->date = $this->_profile->created;
            array_push($feed, $feedItem);

            // Medals
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `medal_id`, `awarded`
                FROM user_medals
                WHERE `user_id` = :id
            ');
            $stmt->execute(array(':id' => $this->id));
            $rows = $stmt->fetchAll();

            foreach($rows AS $row) {
                $feedItem = new \stdClass();
                $feedItem->type = 'medal';
                $feedItem->date = $row->awarded;
                $feedItem->medal = $row->medal_id;
                array_push($feed, $feedItem);
            }

            // Levels
            $stmt = \dtw\DtW::$db->prepare('
                SELECT level_id, `finished`
                FROM `user_levels`
                WHERE `user_id` = :ID AND `finished` IS NOT NULL
                ORDER BY `finished` DESC
            '); 
            $stmt->execute(array(':ID' => $this->id));
            $rows = $stmt->fetchAll();

            foreach($rows AS $row) {
                $feedItem = new \stdClass();
                $feedItem->type = 'level';
                $feedItem->date = $row->finished;
                $feedItem->level = $row->level_id;
                array_push($feed, $feedItem);
            }

            // Articles
            $stmt = \dtw\DtW::$db->prepare('
                SELECT article_id, `published`
                FROM `articles`
                WHERE `author` = :ID AND `status` = "published"
                ORDER BY `published` DESC
            '); 
            $stmt->execute(array(':ID' => $this->id));
            $rows = $stmt->fetchAll();

            foreach($rows AS $row) {
                $feedItem = new \stdClass();
                $feedItem->type = 'article';
                $feedItem->date = $row->published;
                $feedItem->article = $row->article_id;
                array_push($feed, $feedItem);
            }

            // Usernames
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `username`, `changed`
                FROM user_usernames
                WHERE `user_id` = :ID
            '); 
            $stmt->execute(array(':ID' => $this->id));
            $rows = $stmt->fetchAll();

            foreach($rows AS $row) {
                $feedItem = new \stdClass();
                $feedItem->type = 'username';
                $feedItem->date = $row->changed;
                $feedItem->username = $row->username;
                array_push($feed, $feedItem);
            }

            // Discussions
            // $stmt = \dtw\DtW::$db->prepare('
            //     SELECT post_id, thread_id, `posted`
            //     FROM `forum_thread_posts`
            //     WHERE `author` = :ID AND `deleted` != 1
            // '); 
            // $stmt->execute(array(':ID' => $this->id));
            // $rows = $stmt->fetchAll();

            // foreach($rows AS $row) {
            //     $feedItem = new \stdClass();
            //     $feedItem->type = 'discussion';
            //     $feedItem->date = $row->posted;
            //     $feedItem->thread = $row->thread_id;
            //     $feedItem->post = $row->post_id;
            //     array_push($feed, $feedItem);
            // }

            usort($feed, function($a, $b) {
                return strtotime($b->date) - strtotime($a->date);
            });

            \dtw\DtW::$redis->set($key, json_encode($feed));
            \dtw\DtW::$redis->expire($key, 300);

            return $feed;
        }

        private function loadProfile() {
            // Is it cached?
            $profile = \dtw\DtW::$redis->get('user:' . $this->id);
            if ($profile) {
                $this->_profile = json_decode($profile);
                \dtw\DtW::$log->info('user.profile', array('id' => $this->id, 'source' => 'redis'));
            } else {
                \dtw\DtW::$log->info('user.profile', array('id' => $this->id, 'source' => 'DB'));

                $this->_profile = new \stdClass();

                $stmt = \dtw\DtW::$db->prepare('
                    SELECT username, created, reputation, privileges
                    FROM users
                    WHERE users.`user_id` = :id'); 
                $stmt->execute(array(':id' => $this->id)); 
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();

                    $this->_profile = $row;

                    $this->_profile->permalink = \dtw\DtW::$config->get('site.domain') .  '/profile/' . strtolower($this->_profile->username);

                    $this->_profile->feedVisible = true;
                    $this->_profile->medalsVisible = true;
                    $this->_profile->followersVisible = true;

                    $stmt = \dtw\DtW::$db->prepare('
                        SELECT `key`, `value`
                        FROM user_profile_meta
                        WHERE `user_id` = :id'); 
                    $stmt->execute(array(':id' => $this->id)); 
                    if ($stmt->rowCount()) {
                        $meta = $stmt->fetchAll();

                        foreach($meta AS $row) {
                            $value = $row->value;

                            if ($row->key === 'website') {
                                $value = \dtw\utils\Utils::validateURL($value);
                            }

                            $this->_profile->{$row->key} = $value;
                        }
                    }
                } else {
                    throw new \Exception('Profile not found');
                }
                
                $this->updateCache();
            }
        }

        private function loadStats() {
            // Get stats
            $this->_profile->stats = new \stdClass();
            $stmt = \dtw\DtW::$db->prepare('
                SELECT count(`user_id`)
                FROM `user_levels`
                INNER JOIN `levels`
                ON `levels`.`level_id` = `user_levels`.`level_id` AND `status` = "live"
                WHERE `user_id` = :ID AND `finished` IS NOT NULL
            '); 
            $stmt->execute(array(':ID' => $this->id));
            $this->_profile->stats->levelsCompleted = $stmt->fetch(\PDO::FETCH_COLUMN);

            // $this->getLevelProgress();
            // $this->calculateReputation();

            // Count number of users followed by this profile
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `following`
                FROM `user_following`
                WHERE `user_id` = :ID
            '); 
            $stmt->execute(array(':ID' => $this->id));
            $this->_profile->following = $stmt->fetchAll(\PDO::FETCH_COLUMN);
            $this->_profile->followingCount = count($this->_profile->following);

            // Count number of users following this profile
            $stmt = \dtw\DtW::$db->prepare('
                SELECT count(*)
                FROM `user_following`
                WHERE `following` = :ID
            '); 
            $stmt->execute(array(':ID' => $this->id));
            $this->_profile->followersCount = $stmt->fetch(\PDO::FETCH_COLUMN);

            $this->updateCache();
        }

        public function getLevels($force = true) {
            if (!$force && isset($this->_profile->levels) && $this->_profile->levels) {
                return (array) $this->_profile->levels;
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT `user_levels`.level_id, `finished`
                FROM `user_levels`
                INNER JOIN `levels`
                ON `levels`.`level_id` = `user_levels`.`level_id` AND `status` = "live"
                WHERE `user_id` = :ID AND `finished` IS NOT NULL
                ORDER BY `finished` DESC
            '); 
            $stmt->execute(array(':ID' => $this->id));
            $levels = $stmt->fetchAll();

            $this->_profile->levels = array();
            foreach($levels AS $level) {
                $this->_profile->levels[$level->level_id] = $level;
            }

            if ($this->_profile->stats) {
                $this->_profile->stats->levelsCompleted = count($levels);
            }

            $this->updateCache();

            return $levels;
        }

        private function getLevelProgress() {
            if (isset($this->_profile->levelProgress) && $this->_profile->levelProgress) {
                return $this->_profile->levelProgress;
            }

            if (!$this->_profile->stats->levelsCompleted) {
                $this->loadStats();
            }
            $levelCount = \dtw\Playground::getLevelCount();

            if (!$this->_profile->stats->levelsCompleted || $this->_profile->stats->levelsCompleted == 0) {
                $levelProgress = 0;
            } else {
                $levelProgress = ($this->_profile->stats->levelsCompleted / $levelCount) * 100;
            }

            $this->_profile->levelProgress = $levelProgress;

            $this->updateCache();

            return $levelProgress;
        }

        public function getForumActivity() {
            if (isset($this->_profile->forumActivity) && $this->_profile->forumActivity) {
                return $this->_profile->forumActivity;
            }

            $forumActivity = new \stdClass();

            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) FROM forum_threads WHERE author = :ID AND `deleted` = 0');
            $stmt->execute(array(':ID' => $this->id));
            $forumActivity->threads = $stmt->fetch(\PDO::FETCH_COLUMN);

            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) FROM forum_thread_posts WHERE author = :ID AND `deleted` = 0');
            $stmt->execute(array(':ID' => $this->id));
            $forumActivity->posts = $stmt->fetch(\PDO::FETCH_COLUMN);

            $this->_profile->forumActivity = $forumActivity;

            $this->updateCache();

            return $forumActivity;
        }

        public function isPrivacyEnabled($key) {
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `value`
                FROM user_settings
                WHERE `key` = :key AND `user_id` = :id'); 
            $stmt->execute(array(
                ':id' => $this->id,
                ':key' => 'privacy.' . $key
            )); 
            $setting = $stmt->fetch(\PDO::FETCH_COLUMN);

            return $setting === '1';
        }

        // Is the logged in user following this profile
        public function beingFollowed() {
            $DtW = \dtw\DtW::getInstance();

            if (!$DtW->user->isAuth() || $this->currentUser) {
                return false;
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT count(*)
                FROM `user_following`
                WHERE `user_id` = :id AND `following` = :following
                LIMIT 1
            '); 
            $stmt->execute(array(
                ':following' => $this->id,
                ':id' => $DtW->user->id
            ));
            return !!$stmt->fetch(\PDO::FETCH_COLUMN);
        }

        // Is the logged in user following this profile
        public function isFollowing($userID = null) {
            $DtW = \dtw\DtW::getInstance();

            if (!$DtW->user->isAuth() || $this->currentUser) {
                return false;
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT count(*)
                FROM `user_following`
                WHERE `user_id` = :id AND `following` = :following
                LIMIT 1
            '); 
            $stmt->execute(array(
                ':following' => $userID ? $userID : $DtW->user->id,
                ':id' => $this->id
            ));
            return !!$stmt->fetch(\PDO::FETCH_COLUMN);
        }

        public function follow() {
            if ($this->currentUser) {
                throw new \Exception("That's a bit vain");
            }

            $loggedInUserID = \dtw\DtW::getInstance()->user->id;
            if ($this->isBlocked($loggedInUserID) || !$this->_profile->followersVisible) {
                throw new \Exception($this->username . " has blocked followers");
            }


            $stmt = \dtw\DtW::$db->prepare('INSERT IGNORE INTO `user_following` (`user_id`, `following`) VALUES (:id, :following)'); 
            $stmt->execute(array(
                ':following' => $this->id,
                ':id' => $loggedInUserID
            ));

            if ($stmt->rowCount()) {
                // Update this users follower count
                \dtw\Users::purgeUser($this->id);

                // Update logged in users following count
                \dtw\Users::purgeUser($loggedInUserID);

                // Check medals
                \dtw\Medals::checkUsersMedals('following', $loggedInUserID);
                \dtw\Medals::checkUsersMedals('followers', $this->id);

                // Notify user
                \dtw\Notifications::send($this->id, 'profile.follow');
            }
        }

        public function unfollow() {
            if ($this->currentUser) {
                throw new \Exception("That's a bit vain");
            }

            $loggedInUserID = \dtw\DtW::getInstance()->user->id;

            $stmt = \dtw\DtW::$db->prepare('DELETE FROM `user_following` WHERE `user_id` = :id AND `following` = :following');
            $stmt->execute(array(
                ':following' => $this->id,
                ':id' => $loggedInUserID
            ));

            if ($stmt->rowCount()) {
                // Update this users follower count
                \dtw\Users::purgeUser($this->id);

                // Update logged in users following count
                \dtw\Users::purgeUser($loggedInUserID);

                // Check medals
                \dtw\Medals::checkUsersMedals('following', $loggedInUserID);
                \dtw\Medals::checkUsersMedals('followers', $this->id);
            }
        }

        public function getFollowers() {
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `user_following`.`user_id`
                FROM `user_following`
                INNER JOIN `users`
                    ON `users`.`user_id` = `user_following`.`user_id`
                WHERE `user_following`.`following` = :id
                ORDER BY `users`.`username`
            '); 
            $stmt->execute(array(
                ':id' => $this->id
            ));

            return $stmt->fetchAll(\PDO::FETCH_COLUMN);
        }

        public function getFollowing() {
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `following`
                FROM `user_following`
                INNER JOIN `users`
                    ON `users`.`user_id` = `user_following`.`following`
                WHERE `user_following`.`user_id` = :id
                ORDER BY `users`.`username`
            '); 
            $stmt->execute(array(
                ':id' => $this->id
            ));

            return $stmt->fetchAll(\PDO::FETCH_COLUMN);
        }

        public function isBlocked() {
            $DtW = \dtw\DtW::getInstance();

            if (!$DtW->user->isAuth() || $this->currentUser) {
                return false;
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT count(*)
                FROM `user_blocks`
                WHERE `user_id` = :id AND `blocked_id` = :blocked
                LIMIT 1
            '); 
            $stmt->execute(array(
                ':id' => $DtW->user->id,
                ':blocked' => $this->id
            ));
            return !!$stmt->fetch(\PDO::FETCH_COLUMN);
        }

        public function hasBlocked($userID = null) {
            $DtW = \dtw\DtW::getInstance();

            if (!$DtW->user->isAuth() || $this->currentUser) {
                return false;
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT count(*)
                FROM `user_blocks`
                WHERE `user_id` = :id AND `blocked_id` = :blocked
                LIMIT 1
            '); 
            $stmt->execute(array(
                ':id' => $this->id,
                ':blocked' => $userID ? $userID : $DtW->user->id
            ));
            return !!$stmt->fetch(\PDO::FETCH_COLUMN);
        }

        public function block() {
            if ($this->currentUser) {
                throw new \Exception("I wouldn't do that if I was you");
            }

            $loggedInUserID = \dtw\DtW::getInstance()->user->id;

            $stmt = \dtw\DtW::$db->prepare('INSERT IGNORE INTO `user_blocks` (`user_id`, `blocked_id`) VALUES (:id, :blocked)'); 
            $stmt->execute(array(
                ':blocked' => $this->id,
                ':id' => $loggedInUserID
            ));
        }

        public function unblock() {
            if ($this->currentUser) {
                return;
            }

            $loggedInUserID = \dtw\DtW::getInstance()->user->id;

            $stmt = \dtw\DtW::$db->prepare('DELETE FROM `user_blocks` WHERE `user_id` = :id AND `blocked_id` = :blocked');
            $stmt->execute(array(
                ':blocked' => $this->id,
                ':id' => $loggedInUserID
            ));
        }

        public function getRank() {
            if ($this->tempRank) {
                return $this->tempRank;
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT COUNT(*) + 1 AS `rank`, SUM(case when `reputation` = :score then 1 else 0 end) AS `joint`
                FROM users
                LEFT OUTER JOIN `user_settings`
                ON `user_settings`.`user_id` = `users`.`user_id` AND `user_settings`.`key` = "privacy.leaderboard"
                WHERE reputation >= :score AND (`user_settings`.`value` IS NULL OR `user_settings`.`value` = 0)
            ');
            $stmt->execute(array(
                ':score' => $this->_profile->reputation
            ));

            $row = $stmt->fetch();

            $this->tempRank = array(
                'rank' => $row->rank - $row->joint,
                'joint' => $row->joint > 1
            );

            return $this->tempRank;
        }

        public function calculatePoints($force = false) {
            if (!$force && isset($this->_profile->points) && $this->_profile->points) {
                return $this->_profile->points;
            }

            $stmt = \dtw\DtW::$db->prepare("
                SELECT
                    SUM(
                        CASE `difficulty`
                            WHEN 'bronze' THEN 10
                            WHEN 'silver' THEN 25
                            WHEN 'gold' THEN 50
                            ELSE 0 
                        END
                    ) as `points`
                FROM `user_levels`
                INNER JOIN `levels`
                ON `levels`.`level_id` = `user_levels`.`level_id`
                WHERE `user_levels`.`finished` IS NOT NULL
                AND `user_id` = :ID
            ");

            $stmt->execute(array(':ID' => $this->id));
            $points = $stmt->fetchColumn();

            $this->_profile->points = $points;

            // Update DB
            $stmt = \dtw\DtW::$db->prepare('UPDATE users SET `points` = :points WHERE `user_id` = :id');
            $stmt->execute(array(
                ':id' => $this->id,
                ':points' => $points
            ));

            $this->updateCache();
            return $points;
        }

        public function calculateReputation($force = false, $medals = false) {
            if (!$force && isset($this->_profile->reputation) && $this->_profile->reputation) {
                return $this->_profile->reputation;
            }

            $DtW = \dtw\DtW::getInstance();

            // Check users medals
            if ($medals) {
                $this->checkMedals();
            }

            $stmt = \dtw\DtW::$db->prepare(
<<<SQL
    SELECT
        (
            IFNULL(`medals`.`reputation`, 0) +
            IFNULL(`discussion_posts`.`reputation`, 0) + 
            IFNULL(`discussion_threads`.`reputation`, 0) + 
            IFNULL(`discussion_answers`.`reputation`, 0) + 
            IFNULL(`articles`.`reputation`, 0)
        ) AS `total`
    FROM `users`

    -- medals
    LEFT JOIN (
        SELECT
            `user_id`,
            SUM(
                CASE `colour`
                    WHEN 'bronze' THEN 5
                    WHEN 'silver' THEN 15
                    WHEN 'gold' THEN 25
                    ELSE 0 
                END
            ) as `reputation`
        FROM `user_medals`
        INNER JOIN `medals`
        ON `medals`.`medal_id` = `user_medals`.`medal_id`
        WHERE `user_id` = :ID
        GROUP BY `user_id`
    ) `medals`
    ON `medals`.`user_id` = `users`.`user_id`

    -- articles
    LEFT JOIN (
        SELECT
            `author` AS `user_id`,
            count(*) * 25 as `reputation`
        FROM `articles`
        WHERE `status` = "published"
            AND `author` = :ID
        GROUP BY `user_id`
    ) `articles`
    ON `articles`.`user_id` = `users`.`user_id`

    -- discussion posts
    LEFT JOIN (
        SELECT
            `author` AS `user_id`,
            FLOOR(count(*) / 10) as `reputation`
        FROM `forum_thread_posts`
        WHERE `deleted` = 0
            AND `author` = :ID
        GROUP BY `user_id`
    ) `discussion_posts`
    ON `discussion_posts`.`user_id` = `users`.`user_id`

    -- discussion threads
    LEFT JOIN (
        SELECT
            count(*) * 10 AS `reputation`,
            `forum_threads`.`author` AS `user_id`
        FROM forum_threads
        INNER JOIN (
            SELECT `thread_id`, count(*) AS `count`
            FROM forum_thread_posts
            WHERE forum_thread_posts.deleted = 0
            GROUP BY `thread_id`
            HAVING `count` > 10
        ) `posts`
        ON `posts`.`thread_id` = `forum_threads`.`thread_id`
        WHERE forum_threads.deleted = 0
            AND `forum_threads`.`author` = :ID
        GROUP BY `user_id`
    ) `discussion_threads`
    ON `discussion_threads`.`user_id` = `users`.`user_id`

    -- discussion answers
    LEFT JOIN (
        SELECT
            `forum_thread_posts`.`author` AS `user_id`,
            count(*) * 25 as `reputation`
        FROM `forum_thread_posts`
        INNER JOIN forum_threads
            ON forum_thread_posts.post_id = forum_threads.answer
        WHERE `forum_thread_posts`.`author` = :ID
        GROUP BY `user_id`
    ) `discussion_answers`
    ON `discussion_answers`.`user_id` = `users`.`user_id`
    WHERE `users`.`user_id` = :ID
SQL
            );
            $stmt->execute(array(':ID' => $this->id));
            $reputation = $stmt->fetchColumn();


            // Add points from levels
            $points = $this->calculatePoints(true);
            $reputation += $points;

            $this->_profile->reputation = $reputation;

            // Update DB
            $stmt = \dtw\DtW::$db->prepare('UPDATE users SET `reputation` = :reputation WHERE `user_id` = :id');
            $stmt->execute(array(
                ':id' => $this->id,
                ':reputation' => $reputation
            ));

            // Check their privileges
            $privileges = null;
            if ($reputation > 1000) {
                $privileges = 'gold';
            } else if ($reputation > 750) {
                $privileges = 'silver';
            } else if ($reputation > 350) {
                $privileges = 'bronze';
            }

            // Update privileges in DB
            $stmt = \dtw\DtW::$db->prepare('
                UPDATE users
                SET `privileges` = :privileges
                WHERE `user_id` = :id
                    AND (`privileges` IS NULL OR (`privileges` != "black" AND `privileges` != "green"))
            ');
            $stmt->execute(array(
                ':id' => $this->id,
                ':privileges' => $privileges
            ));

            // Store update for user if value has changed
            if ($stmt->rowCount()) {
                $key = 'user:' . $this->id . ':privileges';
                \dtw\DtW::$redis->set($key, $privileges ? $privileges : 'unset');
                \dtw\DtW::$redis->expire($key, 604800);
            }

            $this->updateCache();
            return $reputation;
        }

        public function getUserBar($display = null) {
            // Create objects
            $image = new \Imagick($_SERVER['DOCUMENT_ROOT'] . '/imgs/userbar.png');
            $shine = new \Imagick($_SERVER['DOCUMENT_ROOT'] . '/imgs/userbar-shine.png');
             
            // Create a new drawing palette
            $draw = new \ImagickDraw();

            $draw->setTextKerning(3);
             
            // Set font properties
            $draw->setFont($_SERVER['DOCUMENT_ROOT'] . '/css/fonts/Orbitron-Light.woff');
            $draw->setFontSize(18);
            $draw->setFillColor('white');

            $text = "Defend the Web";
            $draw->setGravity(\Imagick::GRAVITY_WEST);
            $image->annotateImage($draw, 10, 2, 0, $text);

            switch ($display) {
                case 'name': case 'both': $text = $this->name; break;
                default: $text = $this->username;
            }

            $offset = 10;
            if ($display === 'both') {
                $subtext = '[' . $this->username . ']';

                $draw->setFillColor('#ccc');
                $draw->setFontSize(16);
                $draw->setGravity(\Imagick::GRAVITY_EAST);
                $image->annotateImage($draw, 10, 2, 0, $subtext);

                $offset += 10 + $image->queryFontMetrics($draw, $subtext)['textWidth'];
            }

            $draw->setFillColor('#fff');
            $draw->setFontSize(18);
            $draw->setGravity(\Imagick::GRAVITY_EAST);
            $image->annotateImage($draw, $offset, 2, 0, $text);

            // Add shine
            for ($n = 0; $n < 8; $n++) {
                $image->compositeImage($shine, \Imagick::COMPOSITE_DEFAULT, 0, 0);
            }
             
            $image->resizeImage(350, 20, \Imagick::FILTER_CATROM, 0.5);

            // Set output image format
            $image->setImageFormat('png');
             
            // Output the new image
            return $image;
        }

        private function updateCache($create = true) {
            $key = 'user:' . $this->id;


            if (!$create && !\dtw\DtW::$redis->get($key)) {
                return;
            }

            \dtw\DtW::$redis->set($key, json_encode($this->_profile));

            // Set expiry
            \dtw\DtW::$redis->expire($key, 900);
        }

        public function deleteCache() {
            $key = 'user:' . $this->id;
            \dtw\DtW::$redis->del($key);

            // Clear feed
            \dtw\DtW::$redis->del($key . ':feed');
        }

        public function buildSearch() {
            \dtw\DtW::getInstance()->load('Search');

            $params = ['index' => 'users', 'id' => $this->id, 'body' => ['username' => $this->username]];
            try {
                \dtw\DtW::getInstance()->search->client->index($params);
            } catch(\Exception $e) {
                //
            }
        }

    }