/* global $, window, console, alert, document */

document.documentElement.className = document.documentElement.className.replace("no-js","js");

if (typeof $ === 'undefined') {
    var a  = document.createElement('script');
    a.setAttribute('src', '/js/jquery.min.js');
    var b = document.getElementsByTagName('body')[0];
    b.appendChild(a);
}

if (!document.dtw) {
    document.dtw = {};
}
document.dtw.api = "/api/1/";

// Hide cookie dialog
if (document.cookie.indexOf('cookies_dismissed=') !== -1) {
    $('.cookies').remove();
} else{
    $('.cookies .button').on('click', function(e) {
        e.preventDefault();
        document.cookie = 'cookies_dismissed=1; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/';

        $('.cookies').remove();
    });
}

// Setup static content URL
if ($('meta[name="site-static"]').length) {
    document.dtw.static = $('meta[name="site-static"]').attr("content");
}

// Setup CSRF ajax token
if ($('meta[name="csrf-token"]').length) {
    document.dtw.csrf = $('meta[name="csrf-token"]').attr("content");
    $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
        if (options.type.toLowerCase() === "post") {
            if (typeof options.data !== 'object') {
                options.data = options.data ? options.data + "&" : "";
                options.data += "token=" + encodeURIComponent(document.dtw.csrf);
            }
        }
    });

    $('a[csrf]').on('click', function(e) {
        e.preventDefault();

        var form = document.createElement("form");
        form.setAttribute("method", 'post');
        form.setAttribute("action", $(this).attr('href'));

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "token");
        hiddenField.setAttribute("value", document.dtw.csrf);
        form.appendChild(hiddenField);

        document.body.appendChild(form);
        form.submit();
    });
}

// Fix anchor links
function offsetAnchor() {
    if (location.hash.length !== 0) {
        window.scrollTo(window.scrollX, window.scrollY - ($('body > header').height() + 50));
    }
}
$(document).on('click', 'a[href^="#"]', function(event) {
    window.setTimeout(function() {
        offsetAnchor();
    }, 0);
});
window.setTimeout(offsetAnchor, 0);

$(function() {
    "use strict";

    // Style links which contain imgs
    $("a").has("img").addClass("nounderline");

    if (typeof twemoji !== 'undefined') {
        twemoji.parse(document.body, {
          folder: 'svg',
          ext: '.svg'
        });
    }

    var windowHeight = $(window).height(),
        windowWidth = $(window).width();

    $(window).resize(function() {
        windowHeight = $(window).height();
        windowWidth = $(window).width();
    });

    $('[data-back]').on('click', function(e) {
        e.preventDefault();
        window.history.back();
    });

    $("header li:not(.has-children) > a").on("click", function(e) {
        e.stopPropagation();
    });

    $("header li.has-children").on("click", function(e) {
        e.preventDefault();
        $(this)
            .parent()
            .find(".selected")
            .not(this)
            .removeClass("selected");
        $(this).toggleClass("selected");
    });

    $("#sidebar li.has-children")
        .on("mouseenter", function(e) {
            e.preventDefault();
            $(this).addClass("hover");
        })
        .on("mouseleave", function(e) {
            e.preventDefault();
            $(this).removeClass("hover");
        });

    // $('li.has-children > ul').on('click', function(e) {
    //     e.stopPropagation();
    // });

    $('a[data-toggle-dark-mode]').on('click', function(e) {
        e.preventDefault();

        var darkMode = $(this).data('toggle-dark-mode');

        $('html').removeClass('dark-mode');
        if (darkMode) {
            $('html').addClass('dark-mode');
        }

        $.post(document.dtw.api + 'user/update', {
            darkMode: darkMode
        });
    });

    if ($("body").hasClass("home")) {
        $(window).scroll(function() {
            var scrollTop = $(window).scrollTop();

            if (scrollTop > $("#header > .logo").position().top) {
                $("header").addClass("logo-visible");
            } else {
                $("header").removeClass("logo-visible");
            }
        });
    }

    $("[data-scroll-target]").on("click", function(e) {
        e.preventDefault();

        var target = $(this).data("scroll-target");
        $("html, body").animate(
            {
                scrollTop: $(target).offset().top
            },
            400,
            "swing",
            function() {
                $(target).focus();
            }
        );
    });

    $("[data-scroll-top]").on("click", function(e) {
        e.preventDefault();

        $("html, body").animate(
            {
                scrollTop: 0
            },
            400
        );
    });

    $("[data-scroll-bottom]").on("click", function(e) {
        e.preventDefault();

        $("html, body").animate(
            {
                scrollTop: $(document).height()
            },
            400
        );
    });

    $("[data-toggle]").on("click", function(e) {
        e.preventDefault();
        var selector = $(this).data("toggle"),
            $target;

        if ($(this).is('[data-toggle-all]')) {
            $target = $(selector);
        } else if ($(this).siblings(selector).length) {
            $target = $(this).siblings(selector);
        } else if ($(this).children(selector).length) {
            $target = $(this).children(selector);
        } else if ($(this).closest(selector).length) {
            $target = $(this).closest(selector);
        } else {
            $target = $(selector);
        }

        if (!$target.is(':visible') || $(this).is('[data-toggle-show]')) {
            $target.removeClass('hide');
            $target.show();
            $(this).addClass('toggle-visible');
        } else {
            $target.addClass('hide');
            $target.hide();
            $(this).removeClass('toggle-visible');
        }

        if ($(this).is('[data-toggle-show]')) {
            $(this).hide();
        }

        var msg = $(this).data("toggle-text");
        if (msg) {
            $(this).data("toggle-text", $(this).text());
            $(this).text(msg);
        };
    });

    // Click outside element
    $(document).on('mouseup', function(e) {
        var containers = $("[data-toggle-unfocus]");

        containers.each(function() {
            var $selector = $(this).data("toggle"),
                $target;

            if ($(this).siblings($selector).length) {
                $target = $(this).siblings($selector);
            } else {
                $target = $(this).children($selector);
            }
 
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 &&
                !$target.is(e.target) && $target.has(e.target).length === 0
            ) {
                $target.addClass('hide');
                $target.hide();
                $(this).removeClass('toggle-visible');
            }
        });

        // Click outside sidebar menu
        var $target = $("#sidebar"),
            $target2 = $('.show-sidebar');
        if (!$target.is(e.target) && $target.has(e.target).length === 0 &&
            !$target2.is(e.target) && $target2.has(e.target).length === 0) {
            $("body").removeClass("sidebar--open");
        }
    });

    // Mobile, show/hide sidebar
    $(".show-sidebar").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("sidebar--open");
    });


    $("[data-hover-text]")
        .on("mouseenter", function(e) {
            $(this).data('original-text', $(this).text());
            $(this).text($(this).data('hover-text'));
        })
        .on("mouseleave", function(e) {
            $(this).text($(this).data('original-text'));
        });


    // Sort ul
    $("[data-sort]").on("click", function(e) {
        var subject = $(this).data('sort'),
            sortOrder = $(this).data('sort-type'),
            $target = $(this).closest('.block').find('ul[data-sortable]');

        subject = '.' + subject;

        var items = $target.children('li').get();
        switch (sortOrder) {
            case 'alphanumeric': items = dataSortAlphanumeric(items, subject); break;
            case 'numeric': items = dataSortNumeric(items, subject); break;
            default:
                if (sortOrder.split(',').length > 1) {
                    items = dataSortOrder(items, subject, sortOrder.split(','));
                }
        }

        $.each(items, function(i, li){
            $target.append(li);
        });

        var $handle = $(this).closest('.button-dropdown-menu').siblings('.button-dropdown-show');
        $(this).closest('.button-dropdown-menu').hide();
        $handle.removeClass('toggle-visible');
        $handle[0].childNodes[0].nodeValue = $(this).text() + ' ';
    });

    function dataSortNumeric(items, subject) {
        return items.sort(function(a, b) {
            var keyA = parseInt($(a).find(subject).text().replace(/\D/g,''));
            var keyB = parseInt($(b).find(subject).text().replace(/\D/g,''));

            if (keyA < keyB) return 1;
            if (keyA > keyB) return -1;
            return 0;
        });
    }

    function dataSortAlphanumeric(items, subject) {
        return items.sort(function(a, b) {
            var keyA = $(a).find(subject).text();
            var keyB = $(b).find(subject).text();

            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });
    }

    function dataSortOrder(items, subject, order) {
        return items.sort(function(a, b) {
            var keyA = $(a).find(subject).text(),
                keyB = $(b).find(subject).text(),
                indexA = order.indexOf(keyA),
                indexB = order.indexOf(keyB);

            if (indexA < indexB) return -1;
            if (indexA > indexB) return 1;
            return 0;
        });
    }

    // Video placeholder
    $('.video--placeholder').on('click', function(e) {
        e.preventDefault();

        var src = 'https://www.youtube-nocookie.com/embed/' + $(this).data('youtube') + '?modestbranding=1&autoplay=1&rel=0',
            $iframe = $('<iframe>', { src: src }),
            $video = $('<div>', { class: 'video' });

        $video.append($iframe);

        $(this).replaceWith($video);

        // Update analytics 
        $.post(document.dtw.api + 'videos/watch', {
            vid: $(this).data('youtube'),
            source: 'youtube'
        });
    });

    // spoilers
    $('blockquote > blockquote').on('click', function() {
        $(this).toggleClass('active');
    }).find('a').on('click', function(e) {
        e.stopPropagation();
    });


    // Donation tiers
    $('a[data-amount]').on('click', function() {
        $('#amount').val($(this).data('amount'));
    });


    // $('nav#sidebar li a').on('click', function(e) {
    //     e.preventDefault();
    //     e.stopPropagation();

    //     var url = $(this).attr("href");
    //     // if ($(this).parent().hasClass('has-children')) {
    //     //     url = $(this).siblings('ul').children(':first-child').children('a').attr('href');
    //     // } else if (!$(this).closest('.has-children').length) {
    //     //     url = '/course/';
    //     // }

    //     highlightSidebarItem(this);

    //     $.get(url).done(function(data) {
    //         $("#content").replaceWith(data.content);
    //         document.title = data.title + ' | Defend the Web';
    //         window.history.pushState({"html": data.content, "url": url, "pageTitle": document.title}, "", url);

    //         // Handle page extras
    //         // CSS
    //         if (data.extra.styles) {
    //             $.each(data.extra.styles, function(a) {
    //                 if (!$("link[href='"+this+"']").length) {
    //                     $("<link href='"+this+"' rel='stylesheet'>").appendTo("head");
    //                 }
    //             });
    //         }

    //         // JS
    //         if (data.extra.scripts) {
    //             $.each(data.extra.scripts, function(a) {
    //                 if (!$("script[src='"+this+"']").length) {
    //                     $("<script src='"+this+"'>").appendTo("head");
    //                 }
    //             });
    //         }
    //     }).fail(function() {
    //         // Something went wrong
    //         document.location = url;
    //     });
    // });

    // window.onpopstate = function(e) {
    //     if (e.state && e.state.length) {
    //         $("#content").replaceWith(e.state.html);
    //         document.title = e.state.pageTitle + " | Defend the Web";

    //         highlightSidebarItem($('nav#sidebar a[href="' + e.state.url + '"]'));
    //     } else {
    //         document.location.reload();
    //     }
    // };

    // function highlightSidebarItem(target) {
    //     $("nav#sidebar li.selected, nav#sidebar li.active").removeClass("active selected");
    //     if (
    //         $(target)
    //             .parent()
    //             .hasClass("has-children")
    //     ) {
    //         $(target)
    //             .parent()
    //             .addClass("selected");
    //         $(target)
    //             .siblings("ul")
    //             .children(":first-child")
    //             .addClass("active");
    //     } else if ($(target).closest(".has-children").length) {
    //         $(target)
    //             .parent()
    //             .addClass("active")
    //             .closest(".has-children")
    //             .addClass("selected");
    //     } else {
    //         $(target)
    //             .parent()
    //             .addClass("selected");
    //     }
    // }

    // View counter
    if ($('.discussion-thread').length && $('.discussion-thread').data('thread-id')) {
        var threadID = $('.discussion-thread').data('thread-id');

        $.post(document.dtw.api + 'discussions/view', {
            id: threadID
        });
    }

    if ($('#content.article').length && $('#content.article').data('article-id')) {
        var id = $('#content.article').data('article-id');

        $.post(document.dtw.api + 'articles/view', {
            id: id
        });
    }
});

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};