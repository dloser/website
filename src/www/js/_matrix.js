var matrixToken = localStorage.getItem('matrixToken');

if (matrixToken) {
    var config = JSON.parse(matrixToken);
    new MatrixClient(config.data);
} else {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/1/user/matrixToken', true);

    request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
            localStorage.setItem('matrixToken', request.responseText);
            var config = JSON.parse(request.responseText);
            new MatrixClient(config.data);
        }
    };

    request.send();
}