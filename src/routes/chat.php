<?php
    $this->respond('GET', '', function($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);

        $args = array();
        $args['breadcrumb'] = array(
            'Chat' => '/chat'
        );

        $macaroon = $app->DtW->user->getMatrixToken('login');

        $args['homeserver'] = 'https://' . \dtw\DtW::$config->get('matrix.homeserver');
        $args['macaroon'] = $macaroon;

        return $app->DtW->tmpl->render('chat.twig', $args);
    });

    $this->respond('GET', '/statistics', function ($request, $response, $service, $app) {
        $breadcrumb = array(
            'Chat' => '/chat',
            'Statistics' => '/chat/statistics',
        );

        $stats = \dtw\Statistics::getChatStats();

        return $app->DtW->tmpl->render('statistics/chat.twig', array('breadcrumb' => $breadcrumb, 'stats' => $stats, 'data' => json_encode($stats)));
    });