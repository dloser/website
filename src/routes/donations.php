<?php
    $this->respond('GET', '', function($request, $response, $service, $app) {
        $args = array();

        $args['breadcrumb'] = array(
            'Donations' => '/donations'
        );

        $args['donations'] = \dtw\Shop::getDonations();

        $donationForm = new \dtw\utils\Form('Join the list! ', "Donate", array(
            'action' => '/shop/donate'
        ));
        $donationForm->addField('product', 'hidden', array(
            'value' => 'donation'
        ));
        $donationForm->addField('Amount', "text", array(
            'prepend' => '£',
            'placeholder' => 'Amount',
            'hideLabel' => true
        ));
        $args['donationForm'] = $donationForm;

        return $app->DtW->tmpl->render('shop/donations.twig', $args);
    });