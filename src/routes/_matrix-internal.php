<?php
    $this->respond('/identity/v1/check_credentials', function ($request, $response, $service, $app) {
        $data = json_decode( file_get_contents( 'php://input' ), true );

        $user = isset($_GET['id']) ? $_GET['id'] : $data['user']['id'];
        // Get username from matrix ID
        $re = '/^@(.*):/m';
        preg_match($re, $user, $matches);
        $user = $matches[1];

        $pass = isset($_GET['password']) ? $_GET['password'] : $data['user']['password'];

        $response = new \stdClass();
        $response->success = false;
        $response->request = $_GET;

        $stmt = \dtw\DtW::$db->prepare('
            SELECT user_id, username, password, email, privileges, twoFactor
            FROM users
            WHERE `username` = :username AND `password` IS NOT NULL
        '); 
        $stmt->execute(array(':username' => $user)); 
        if ($stmt->rowCount()) {
            $row = $stmt->fetch();

            // Is the password correct
            if (password_verify($pass, $row->password)) {
                $response = new \stdClass();
                $response->success = true;
                $response->mxid = "@" . $row->username . ":defendtheweb.co.uk";
                $response->profile = new \stdClass();
                $response->profile->display_name = $row->username;

                $response->profile->three_pids = array();
                $response->profile->three_pids[0] = new \stdClass();
                $response->profile->three_pids[0]->medium = "email";
                $response->profile->three_pids[0]->address = $row->email;
            }
        }

        $auth = new \stdClass();
        $auth->auth = $response;

        echo json_encode($auth);
    });