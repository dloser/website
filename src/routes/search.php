<?php
    $this->respond(function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
    }); 

    $this->respond('GET', '', function($request, $response, $service, $app) {
        $args = array(
            'breadcrumb' => array(
                'Search' => '/search'
            )
        );

        return $app->DtW->tmpl->render('search/results.twig', $args);
    });

    $this->respond('POST', '', function ($request, $response, $service, $app) {
        $app->DtW->load('Search');

        $term = $_POST['q'];

        $args = array(
            'breadcrumb' => array(
                'Search' => '/search',
                $term => '/search'
            ),
            'term' => $term
        );

        try {
            $args['results'] = $app->DtW->search->getResults($term);
        } catch (\Exception $e) {
            $args['exception'] = $e->getMessage();
        }

        return $app->DtW->tmpl->render('search/results.twig', $args);
    });