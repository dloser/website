<?php
    $this->respond('GET', '/[:size]/[:image]', function ($request, $response, $service, $app) {
        $app->DtW->load('Images');
        try {
            $image = $app->DtW->images->resize($request->image, $request->size);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    });