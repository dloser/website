#!/bin/bash

/etc/init.d/elasticsearch start
/etc/init.d/redis-server start
/etc/init.d/mysql start
/etc/init.d/nginx start

php-fpm > /dev/null 2>&1 &

cd /var/www

folders=( "site/staticCache" "logs" "sessions" "site/templates/cache" "uploads" "site/www/imgs/uploads" )
for i in "${folders[@]}"
do
    mkdir -pm 777 $i
    printf "Creating ${i}... ${GREEN}done${NC}\n"
done

# Setup SQL
mysql -u root < sql/setup.sql
mysql -u root dtw < sql/schema.sql
mysql -u root dtw < sql/data.sql

composer install
npm install
npm rebuild node-sass

chmod 777 /var/www/site/vendor/ezyang/htmlpurifier/library/HTMLPurifier/DefinitionCache/Serializer

./node_modules/gulp/bin/gulp.js