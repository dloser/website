### Build Docker image
```
docker build -t dtw_image docker
```

### Create Docker instance from image
```
docker run -d -v ${PWD}/src:/var/www/src \
              -v ${PWD}/sql:/var/www/sql \
              -v ${PWD}/gulpfile.js:/var/www/gulpfile.js \
              -v ${PWD}/package.json:/var/www/package.json \
              -v ${PWD}/composer.json:/var/www/composer.json \
              -v ${PWD}/config/config.json.example:/var/www/config/config.json \
              -v ${PWD}/config/sidebar.json:/var/www/config/sidebar.json \
              -v ${PWD}/config/news.json:/var/www/config/news.json \
              -v ${PWD}/config/usernames.txt:/var/www/config/usernames.txt \
              -p 80:80 \
              -p 3306:3306 \
              --name dtw dtw_image
```

### Update `/etc/hosts`
```
127.0.0.1     dtw.local
127.0.0.1     dtwstatic.local
```

Open [dtw.local](http://dtw.local/). If you get 403 or "connection was reset" the system is still being setup, this might take 5 minutes or so.

### Restart instance
```
docker start dtw
```

### Stop instance
```
docker start dtw
```

### Drop into running instance
```
docker exec -t -i dtw /bin/bash
```